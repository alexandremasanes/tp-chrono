import React, { Component } from 'react';
import './App.css';
import {setSequence, nextId} from "./utils";
import {MOCK_TIMERS} from "./mock-data";
import TimerList from "./TimerList/TimerList";
import TimerForm from "./TimerForm/TimerForm";

export default class App extends Component {

    state = {
        timers: MOCK_TIMERS,
    }

    addTicket = ticket =>
        this.setState({
            ...this.state,
            timers: [
                ...this.state.timers,
                {
                    id: nextId(),
                    ...ticket
                }
            ]
        })

    constructor() {
        super()

        setSequence(
            MOCK_TIMERS.sort(
                (a, b) => a.id - b.id
            )[MOCK_TIMERS.length - 1].id
        )
    }

    render() {
        return (
            <div className="container">
                <div>
                    <header>
                        <h1>
                            Timers
                        </h1>
                    </header>
                    <TimerForm addTicket={this.addTicket}/>
                </div>
                <hr/>
                <div className="row">
                    <TimerList timers={this.state.timers}/>
                </div>
            </div>
    );
  }
}
